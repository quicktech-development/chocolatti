$(document).ready(function() {
	var largura = $(window).width();

	if (largura>1400){
		// Obter altura da tela
		var altura = $(window).height();
		altura = altura - (213 + 12);
		
		//definir altura das páginas
		$('.bxslider-casa li').css('height', altura);
		$('.content-casa').css('height', altura);
	}
	else{
		altura = 650;
		//definir altura das páginas
		$('.bxslider-casa li').css('height', altura);
		$('.content-casa').css('height', altura);

		$('footer').css({
			'position':'fixed',
			'bottom':'-60px'
		});

		$(window).scroll(function(e){
			var altura2 = $(window).height();
			var bot = $("#bottom");
			var pos = bot.position();
			console.log(pos.top);
			console.log("Altura: " + altura2);

			var scrolltop = $(window).scrollTop();

			var max = pos.top - altura2 - 62;
			if( scrolltop > max ){
				$('footer').css({
					'position': 'relative',
					'bottom': '0px'
				});
			}
			else {
				$('footer').css({
					'position': 'fixed',
					'bottom': '-60px'
				});
			}
		});
	}
	
	$(".bxslider-casa").bxSlider({
		mode: "horizontal",
		auto: true,
		autoHover: true,
		captions: true
	});
});