$(document).ready(function() {
	var largura = $(window).width();

	if (largura>1400){
		// Obter altura da tela
		var altura = $(window).height();
		altura = altura - (213 + 12);

		//definir altura das páginas
		$('.bxslider li').css('height', altura);
		$('.bxslider-historia li').css('height', (altura - 200));
		$('#carousel li').css('height', (altura - 250));
		$('.content-produto').css('height', altura);
		$('.content-chocolate').css('height', altura);
		$('.content-historia').css('height', altura);
		$('.content-contato').css('height', altura);
	}
	else{
		altura = 650;
		$('.bxslider li').css('height', altura);
		$('.bxslider-historia li').css('height', (altura - 200));
		$('#carousel li').css('height', (altura - 250));
		$('.content-produto').css('height', altura);
		$('.content-chocolate').css('height', altura);
		$('.content-historia').css('height', altura);
		$('.content-contato').css('height', altura);

		$('footer').css({
			'position':'fixed',
			'bottom':'-60px'
		});

		$(window).scroll(function(e){
			var altura2 = $(window).height();
			var bot = $("#bottom");
			var pos = bot.position();
			console.log(pos.top);
			console.log("Altura: " + altura2);

			var scrolltop = $(window).scrollTop();

			var max = pos.top - altura2 - 62;
			if( scrolltop > max ){
				$('footer').css({
					'position': 'relative',
					'bottom': '0px'
				});
			}
			else {
				$('footer').css({
					'position': 'fixed',
					'bottom': '-60px'
				});
			}
		});
	}



	$("#news-form").popover({
		placement : 'bottom', // top, bottom, left or right
		html: 'true', 
		content : '<div class="title">Receba nossa newsletter:</div><input class="input" type="email" id="news-mail" placeholder="seu email" name="email"><a href="#" id="submit-news" onClick="news()"></a> '
	});

	news = function () {
		var email = $("#news-mail").val();
		if(IsEmail(email)) {
			dados = '{email: ' + email + ' }';
			$.post( "newsletter.php", { email: email } )
			.fail(function(data) {
				alert( data );
				$('#news-mail').val('');
			}).done(function(data){
				alert(data);
				$('#news-mail').val('');
			});
		} else {
			alert("Digite um email válido")
		}
		
	}

	function IsEmail(email){
		er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		if(er.exec(email)) {
			return true;
		} else {
			return false;
		}
	}

});