
relative_assets = true
project_path = File.dirname(__FILE__) + '/'

sass_dir = "sass"
css_dir = "css"
fonts_dir = "fonts"
images_dir = "img"
javascripts_dir = "js"

line_comments = true